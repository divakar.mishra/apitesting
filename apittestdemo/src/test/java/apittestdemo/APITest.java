package apittestdemo;

import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import static io.restassured.RestAssured.given;

import java.util.List;

import io.restassured.response.Response;

public class APITest {
	@Test
	public void getAPITestMethod() {
			System.out.println("GET");
			

			Response response = given()
								.when()
								.get("https://sv443.net/jokeapi/v2/formats");
			
			Formats format = response.getBody().as(Formats.class);
			List<String> formats = format.getFormats();
			String error = format.getError();
			String timestamp = format.getTimestamp();
			
			System.out.println("Error :"+error);
			System.out.println("Formats :"+formats);
			System.out.println("Time Stamp :"+timestamp);
		
	}

	@Test
	public void PutAPITestMethod() {
		try {
		System.out.println("PUT");

		Flags flag = new Flags();
		flag.setNsfw(false);
		flag.setPolitical(true);
		flag.setRacist(false);
		flag.setReligious(false);
		flag.setSexist(false);
		
		Jokes joke = new Jokes();
		joke.setFormatVersion(2);
		joke.setCategory("Programming");
		joke.setType("single");
		joke.setFlags(flag);
		joke.setJoke("This is my Programming Joke");
		
		ObjectMapper objectMapper = new ObjectMapper();
		String body = objectMapper.writeValueAsString(joke);
		
		Response response = given()
							.when()
							.body(body)
							.put("https://sv443.net/jokeapi/v2/submit");
		
		SubmitJokeResponse jokeResponse = response.getBody().as(SubmitJokeResponse.class);
		String error = jokeResponse.getError();
		String message = jokeResponse.getMessage();
		Submission submission = jokeResponse.getSubmission();
		String timestamp = jokeResponse.getTimestamp();
		
		System.out.println("Error :"+error);
		System.out.println("Message :"+message);
		System.out.println("Submission :"+submission);
		System.out.println("Timestamp :"+timestamp);
		
		
		}catch(Exception e) {
			System.out.println("Exception :"+e);
		}
		
	}
}
